package com.example.bruno.newsapp.utils

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
public class MyAppGlideModule : AppGlideModule()